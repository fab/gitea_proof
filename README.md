# KeyOxide.org

This is an OpenPGP proof that connects my OpenPGP key to this Gitea account. For details check out https://keyoxide.org/guides/openpgp-proofs

# Identity

My Online Accounts can be verified here:
https://keyoxide.org/hkp/fab@redterminal.org
